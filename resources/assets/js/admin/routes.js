import VueRouter from "vue-router";

let routes = [
  {
    path: "/login",
    component: require("./components/Login.vue")
  },
  {
    path: "/dashboard",
    component: require("./components/Dashboard.vue"),
    meta: { middlewareAuth: true }
  }
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.middlewareAuth)) {
    if (!auth.check()) {
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });

      return;
    }
  }

  next();
});

export default router;
