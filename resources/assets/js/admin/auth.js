class Auth {
  constructor() {
    this.token = window.localStorage.getItem("token");
    this.refresh = window.localStorage.getItem("refresh");

    let userData = window.localStorage.getItem("user");
    this.user = userData ? JSON.parse(userData) : null;

    if (this.token) {
      axios.defaults.headers.common["Authorization"] = "Bearer " + this.token;
    }
  }

  login(token, refresh, user) {
    window.localStorage.setItem("token", token);
    window.localStorage.setItem("refresh", refresh);
    window.localStorage.setItem("user", JSON.stringify(user));

    axios.defaults.headers.common["Authorization"] = "Bearer " + token;

    this.token = token;
    this.refresh = refresh;
    this.user = user;

    Event.$emit("userLoggedIn");
  }

  logout() {
    window.localStorage.clear();

    this.token = this.refresh = this.user = null;
  }

  check() {
    return !!this.token;
  }
}

export default Auth;
