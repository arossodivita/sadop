import VueRouter from "vue-router";
import router from "./admin/routes.js";
import Auth from "./admin/auth.js";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
window.auth = new Auth();

Vue.use(VueRouter);

window.Event = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("vue-layout", require("./admin/components/Layout.vue"));

Vue.component("login", require("./admin/components/Login.vue"));

Vue.component("dashboard", require("./admin/components/Dashboard.vue"));

const app = new Vue({
  el: "#app",

  router
});
