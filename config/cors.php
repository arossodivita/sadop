<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Headers
    |--------------------------------------------------------------------------
    |
    | Handle Cross-Origin-Resource headers returned for Cord Middleware
    |
    */

	'headers' => [
		'Access-Control-Allow-Origin' => '*',
		'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, PATCH, DELETE',
		'Access-Control-Allow-Headers' => 'authorization,content-type, Origin, Access-Control-Request-Method, Access-Control-Allow-Origin,Access-Control-Allow-Credentials',
		'Access-Control-Allow-Credentials' => true,
	],
];