<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Roles
	|--------------------------------------------------------------------------
	|
	| System roles type names
	|
	*/

	'ROLES' => [
		'GUESS' => 'Invitado',
		'GENERAL_ADMIN' => 'Administrador General',
		'SECRETARY_ADMIN' => 'Administrador de Secretaría',
		'AFFILIATE' => 'Afiliado',
		'TEACHER' => 'Profesor',
		'STUDENT' => 'Alumno',
	],

	'OAUTH' => [
		'GRANT_TYPE' => [
			'PASSWORD' => env('OAUTH_GRANT_TYPE_PASSWORD'),
			'REFRESH_TOKEN' => env('OAUTH_GRANT_TYPE_REFRESH_TOKEN'),
			],
		'CLIENT_ID' => env('OAUTH_CLIENT_ID'),
		'CLIENT_SECRET' => env('OAUTH_CLIENT_SECRET'),
	],
];