<?php

namespace App;

use Illuminate\Support\Facades\Config;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'password', 'dni', 'affiliate_number', 'school', 'picture', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	//Mutators
	public function getPictureAttribute($value)
	{
		return (!empty($value) ? url('/').Storage::url($value) : $value);
	}

	public function getRoleAttribute($value)
	{
		return Config::get('constants.ROLES.'.$value);
	}
}
