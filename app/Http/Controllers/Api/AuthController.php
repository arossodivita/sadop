<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
	/**
	 * Create new User
	 *
	 * @param Request $request
	 */
    public function register(Request $request)
	{
		$validator = $this->validator($request->all());

		if ($validator->fails()) {
			return Response()->json($validator->errors(), 400);
		}

		$data = $request->all();

		$data['password'] = Hash::make($data['password']);

		User::create($data);

		return Response()->json('success', 201);
	}

	public function login(Request $request)
	{ 
		$user = User::whereEmail($request->get('username'))->first();

		if (!$user) {
			return Response()->json(['message' => 'User Not Found'], 404);
		}

		$data = [
			'grant_type' => Config::get('constants.OAUTH.GRANT_TYPE.PASSWORD'),
			'client_id' => Config::get('constants.OAUTH.CLIENT_ID'),
			'client_secret' => Config::get('constants.OAUTH.CLIENT_SECRET'),
			'username' => $request->get('username'),
			'password' => $request->get('password'),
		];

		$response = $this->handleOauthResponse(
			app()->handle(Request::create('/oauth/token', 'POST', $data))
		);

		return Response()->json(
			$response ? [
				'access_token' => $response->access_token,
				'refresh_token' => $response->refresh_token,
				'user' => $user->toArray()
				] : ['message' => 'Unauthorized']
			, 
			$response ? 200 : 401
		);
	}

	public function logout(Request $request)
	{
		DB::table('oauth_refresh_tokens')
			->where('access_token_id', $request->user()->token()->id)
			->update([
				'revoked' => true,
			]);

		$request->user()->token()->revoke();

		return Response()->json('success', 200);
	}

	public function refreshToken(Request $request)
	{
		$data = [
			'grant_type' => Config::get('constants.OAUTH.GRANT_TYPE.REFRESH_TOKEN'),
			'client_id' => Config::get('constants.OAUTH.CLIENT_ID'),
			'client_secret' => Config::get('constants.OAUTH.CLIENT_SECRET'),
			'refresh_token' => $request->get('refresh_token'),
		];

		$request = Request::create('/oauth/token', 'POST', $data);

		return app()->handle($request);
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'surname' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
			'dni' => 'required|numeric|digits_between:7,8|unique:users',
			'affiliate_number' => 'nullable|numeric|unique:users',
			'school' => 'nullable|string|max:255',
			'role' => 'required|string|max:255',
		]);
	}

	private function handleOauthResponse($response)
	{
		// Check if the request was successful
		if ($response->getStatusCode() != 200) {
			return null;
		}
	
		// Get the data from the response
		return json_decode($response->getContent());		
	}
}
