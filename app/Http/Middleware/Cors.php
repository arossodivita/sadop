<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$headers = Config::get('cors.headers');

		$response = $next($request);

		foreach ($headers as $key => $value) {
			$response->headers->set($key, $value);
		}

		return $response;
    }
}
