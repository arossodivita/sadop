<p align="center">PLataforma de cursos web de Sadop - Provincia de Santa Fe, Argentina</p>

## Acerca de

La plataforma de cursos web de Sadop de la Provincia de Santa Fe contempla las siguientes características:

- Registro de usuarios para las localidades de Santa Fe Capital y Rosario.
- Publicación de cursos en línea por cada localidad.
- Descarga de materiales didácticos.
- Chat interno.
